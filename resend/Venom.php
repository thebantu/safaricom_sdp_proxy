<?php
require_once './vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Venom {
    /*
       $this->exchange = 'RETRY_DEAD_EXCHANGE';
       $this->queue = 'RETRY_DEAD_QUEUE';
       $this->worker_exchange = 'DEAD_EMALI5_EXCHANGE';
       $this->worker_route = 'DEAD_EMALI5_ROUTE';


     */
    var $port ='5672';
    var $vhost = '/';
    var $pass ='toor123!';
    var $user = 'bobby';
    var $host = '172.19.1.3';
    var $exchange = 'CONTENT_DEAD_EXCHANGE';
    var $queue = 'CONTENT_DEAD_QUEUE';
    var $worker_exchange = 'SAFARICOM_DLR_POLL_EXCHANGE';
    var $worker_route = 'SAFARICOM_DLR_POLL_ROUTE';

    public function actionPublish($message) {
        //$this->info($message);
        try {
            $connectionection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->pass, $this->vhost);
 //$connectionection->pconnect();        
    $channel = $connectionection->channel();

            $channel->queue_declare($this->queue, false, true, false, false,false,
                    array('x-message-ttl'=>array('I',60000),
                        'x-dead-letter-exchange'=>array('S',$this->worker_exchange),
                        'x-dead-letter-routing-key'=>array('S',$this->worker_route)));
            $channel->exchange_declare($this->exchange, 'direct', false, true, false);
            $channel->queue_bind($this->queue, $this->exchange);


            $outMsg = new AMQPMessage($message, array('delivery_mode' => 1));
            //$this->info("About to publish the message to queue: [ " . $this->queue . " ]");
            $channel->basic_publish($outMsg, $this->exchange);
        } catch (\PhpAmqpLib\Exception\AMQPProtocolConnection $e) {
            $this->info('[Exception\AMQPProtocolConnection] ' . $e->getMessage());
            $this->info("Error creating AMQP connections");
            return FALSE;
        }
        catch(Exception $er){
           $this->info('[Exception] ' . $er->getMessage());
        }
        $channel->close();
        $connectionection->close();
        return TRUE;

    }
    function info($data){
        $file = "info.log";
        date_default_timezone_set("Africa/Nairobi");
        $date = date("Y-m-d H:i:s");
        if ($fo = fopen($file, 'ab')) {
            fwrite($fo, "$date - " . $_SERVER['PHP_SELF'] . ": | $data \n");
            fclose($fo);
        } else {
            trigger_error("flog Cannot log '$data' to file '$file' ", E_USER_WARNING);
        }

    }


}


?>
