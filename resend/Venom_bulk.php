<?php
require_once './vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Venom {
/*
$this->exchange = 'RETRY_DEAD_EXCHANGE';
                $this->queue = 'RETRY_DEAD_QUEUE';
                $this->worker_exchange = 'DEAD_EMALI5_EXCHANGE';
                $this->worker_route = 'DEAD_EMALI5_ROUTE';


*/
var $port ='5672';
var $vhost = '/';
var $pass ='toor123!';
var $user = 'bobby';
var $host = '172.31.186.203';
var $exchange = 'BULK_DEAD_EXCHANGE';
var $queue = 'BULK_DEAD_QUEUE';
var $worker_exchange = 'SAFARICOM_BULK_DLR_POLL_EXCHANGE';
var $worker_route = 'SAFARICOM_BULK_DLR_POLL_ROUTE';
 
public function actionPublish($message) {
$startT = $this->startTimer();
$this->info($message);
               try {
                        $connectionection = new AMQPConnection($this->host, $this->port, $this->user, $this->pass, $this->vhost);
 //$connectionection->pconnect();                                
$channel = $connectionection->channel();
                        $channel->queue_declare($this->queue, false, true, false, false,false,
                                        array('x-message-ttl'=>array('I',60000),
                                                'x-dead-letter-exchange'=>array('S',$this->worker_exchange),
                                                'x-dead-letter-routing-key'=>array('S',$this->worker_route)));
                        $channel->exchange_declare($this->exchange, 'direct', false, true, false);
                        $channel->queue_bind($this->queue, $this->exchange);
                        $outMsg = new AMQPMessage($message, array('delivery_mode' => 2));
                        $channel->basic_publish($outMsg, $this->exchange);
                        $channel->close();
                        $connectionection->close();
$stopT = $this->stopTimer();
$this->info("TOOK ".$stopT." TO Publish message to Q");
                        return TRUE;
                } catch (\PhpAmqpLib\Exception\AMQPProtocolConnection $e) {
                        $this->info('[Exception\AMQPProtocolConnection] ' . $e->getMessage());
                        $this->info("Error creating AMQP connections");
                        return FALSE;
                }
               catch(Exception $er){
                  $this->info('[Exception] ' . $er->getMessage());
               }

}
function startTimer() {
    $mtime = microtime();
    $mtime_r = explode(" ", $mtime);
    $time = $mtime_r[1] + $mtime_r[0];
    return $time;
}

function stopTimer($start) {
    $mtime = microtime();
    $mtime_r = explode(" ", $mtime);
    $time = $mtime_r[1] + $mtime_r[0];
    $endtime = $time;
    $totaltime = ($endtime - $start);
    return $totaltime;
}

function info($data){
    $file = "bulk_info.log";
    date_default_timezone_set("Africa/Nairobi");
    $date = date("Y-m-d H:i:s");
    if ($fo = fopen($file, 'ab')) {
        fwrite($fo, "$date - " . $_SERVER['PHP_SELF'] . ": | $data \n");
        fclose($fo);
    } else {
        trigger_error("flog Cannot log '$data' to file '$file' ", E_USER_WARNING);
    }

}


}


?>
