<?php
//include './const.php';
define("SUCCES" ,200);
define("ERROR", 100);
class dbase {
    var $db_conf;

     function __construct() {
//echo "I am here \n";
$this->parseini();
//print_r($this->db_conf);

     }

function conn($db){

     $conn = new mysqli($this->db_conf['host'], $this->db_conf['username'], $this->db_conf['password'],$this->db_conf[$db]);
return $conn;

}




 function parseini() {
$file = "db.ini";
        $dbsettings = array();
        if (!$dbsettings = parse_ini_file($file, TRUE)):
            throw new exception('Unable to open ' . $file . '.');
        else:
            $this->db_conf = $dbsettings;

        endif;
    }
     
     
function select($query){
$conn =$this->conn("transactional"); 
    $mysqlObject = $conn->query($query);
    if($mysqlObject->num_rows < 0){
        $resource['code']=ERROR;
        $resource['error']=  $conn->error;
        $resource['message']="An error occured while loading data";
        $conn->close();
        return $resource;
    } else if($mysqlObject->num_rows == 0){
         $resource['code']=SUCCES;
        $resource['data']=0;
        $resource['message']="No data Found";
        $conn->close();
        return $resource;
    }
    
    else {
        $data = array();
        $x = 0;
        while($row = $mysqlObject->fetch_assoc()){
            foreach ($row as $key => $value) {
                $data[$x][$key]=$value;
            }
            $x++;
        }
        $resource['code']=SUCCES;
        $resource['message']="data found!";
        $resource['data']=$data;
$conn ->close();
        return $resource;
         
    }
        //  $con->close;

}


function insert($query){
   
        $conn =$this->conn("transactional");
        $msqlRes =  $conn->query($query);
        if($msqlRes){
            $resource['code']=SUCCES;
            $resource['message']="Record added";
            $resource['insert_id'] = $conn->insert_id;
            $conn->close();
            return $resource;
        }  else {
        $resource['code']=ERROR;
        $resource['error']=$conn->error;
        $resource['messages']="An error occured while writting to db";
$conn->close();        
return $resource;
        }
   //   $con->close;
        
    
}


function update ($query){
$conn =$this->conn("transactional");
        $msqlRes = $conn->query($query);
        if($msqlRes){
            $resource['code']=SUCCES;
            $resource['message']="Record updated";
            $resource['insert_id'] = $conn->insert_id;
$conn->close();           
 return $resource;
        }  else {
        $resource['code']=ERROR;
        $resource['error']=$conn->error;
        $resource['messages']="An error occured while updating record";
$conn->close();       
 return $resource;
        }
    //  $con->close;
        
    
}


/////////////////////////////
function selectMaster($query){
$master =$this->conn("master");
    $mysqlObject = $master->query($query);
if($mysqlObject->num_rows < 0){
        $resource['code']=ERROR;
        $resource['error']=$master->error;
        $resource['message']="An error occured while loading data";
        $master->close();
        return $resource;
    } else if($mysqlObject->num_rows == 0){
         $resource['code']=SUCCES;
        $resource['data']=0;
        $resource['message']="No data Found";
$master->close();        
return $resource;
    }  else {
        $data = array();
        $x = 0;
        while($row = $mysqlObject->fetch_assoc()){
            foreach ($row as $key => $value) {
                $data[$x][$key]=$value;
            }
            $x++;
        }
        $resource['code']=SUCCES;
        $resource['message']="data found!";
        $resource['data']=$data;
       
$master->close();
        return $resource;
         
    }
         // $con->close;

}

}
