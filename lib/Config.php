<?php
namespace Utils\Configuration;

define("CONF_FILE",'/var/www/html/sdpsendSMS/conf/config.ini');

Class Config {
    static $config = array ();
    public function __construct() {
    }
    /* Use a static method to avoid 
     * the tedium of initializing the Config
     * Class
     */
    public static function get_config() { 
        self::$config = parse_ini_file(CONF_FILE, true);
        return self::$config;
    }
}

