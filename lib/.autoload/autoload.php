<?php

define('ROOT_DIR', '/var/www/html/sdpsendSMS/lib');

$classes_dir = array (
    /* Lib Dir */
    ROOT_DIR . '/',
);

function __autoload($class_name) {
    /* Handle classes with name spaces */
    $class_parts = explode( '\\', $class_name );
    $class = end( $class_parts );
    print "Found Class => " . $class . PHP_EOL;
    global $classes_dir;
    foreach ($classes_dir as $directory) {
        $file = $directory . $class . '.php';
        if (file_exists( $file )) {
            require_once ( $file );
            return;
        }
    }
}
