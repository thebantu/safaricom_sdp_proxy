<?php
/**
 * Sends a message to a queue
 * Author Reuben Paul Wafula
*/
require_once('amqp.inc');

class Venom {
    /*
       $this->exchange = 'RETRY_DEAD_EXCHANGE';
       $this->queue = 'RETRY_DEAD_QUEUE';
       $this->worker_exchange = 'DEAD_EMALI5_EXCHANGE';
       $this->worker_route = 'DEAD_EMALI5_ROUTE';


     */
    var $port ='5672';
    var $vhost = '/';
    var $pass ='toor123!';
    var $user = 'bobby';
    var $host = '172.31.186.203';
    var $exchange = 'CONTENT_DEAD_EXCHANGE';
    var $queue = 'CONTENT_DEAD_QUEUE';
    var $worker_exchange = 'SAFARICOM_DLR_POLL_EXCHANGE';
    var $worker_route = 'SAFARICOM_DLR_POLL_ROUTE';

    public function actionPublish($message) {
        //$this->info($message);
        try {
            $connectionection = new AMQPConnection($this->host, $this->port, $this->user, $this->pass); 
            $ch = $connectionection->channel();
            //$ch->access_request($this->vhost, false, false, true, true);
            $outMsg = new AMQPMessage($message, array('delivery_mode' => 1));
            //$this->info("About to publish the message to queue: [ " . $this->queue . " ]");
            $ch->basic_publish($outMsg, $this->exchange);
        } catch (Exception $e) {
            $this->info('[Exception\AMQPProtocolConnection] ' . $e->getMessage());
            $this->info("Error creating AMQP connections");
            return FALSE;
        }
        $ch->close();
        $connectionection->close();
        return TRUE;

    }
    function info($data){
        $file = "info.log";
        date_default_timezone_set("Africa/Nairobi");
        $date = date("Y-m-d H:i:s");
        if ($fo = fopen($file, 'ab')) {
            fwrite($fo, "$date - " . $_SERVER['PHP_SELF'] . ": | $data \n");
            fclose($fo);
        } else {
            trigger_error("flog Cannot log '$data' to file '$file' ", E_USER_WARNING);
        }

    }


}


?>
