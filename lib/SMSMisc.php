<?php
if (!defined('ROOT')) define('ROOT', '/var/www/html/sdpsendSMS/lib');
require_once(ROOT . '/Db.php');
require_once(ROOT . '/Config.php');

Class SMSMisc extends Db {
    protected $table_name;

    public function __construct ( $table ) {
        parent::__construct();
        $this->table_name = $table;
    }

    protected function create_insert_field_tuple ( $fields ) {
        $fmt = '%s%s%s';
        return sprintf( $fmt, '( ', join(', ', array_keys($fields)), ')');
    }

    protected function create_param_placeholders ( $fields )  {
        $to_param_names = function ( $fld ) {
            return preg_replace('/^/', ":", $fld);
        };
        return join(', ', array_map($to_param_names, array_keys($fields)));
    }

    protected function create_params ( $fields ) {
        $params = [];
        $cb = function ( $v, $k ) use ( $fields, &$params) {
            $k = ':' . $k;
            $params[$k] = $v;
        };
        array_walk( $fields, $cb );
        return $params;
    }

    public function log_dlr_request ( $request_details ) {
        /* DB / Table Specific Details */

        $field_tuple_str = $this->create_insert_field_tuple( $request_details );
        $param_placeholders = $this->create_param_placeholders( $request_details );
        $params = $this->create_params( $request_details );

        $query = 'insert into ' . $this->table_name . ' ' .$field_tuple_str .
             ' values ( ' . $param_placeholders . ' )';

        $log_dlr_request = $this->prepare($query);

        foreach ($params as $p => $v) $log_dlr_request->bindValue($p, $v);

        $log_dlr_request->execute();

        if ($log_dlr_request->errorInfo()[2] === null)
            return $log_dlr_request->rowCount();
        else
            return $log_dlr_request->errorInfo()[2];
    }

}
