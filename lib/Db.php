<?php

if (!defined('ROOT')) define('ROOT', '/var/www/html/sdpsendSMS/lib');
require_once(ROOT . '/.autoload/autoload.php');

use \PDO as PDO;
use \Exception as Exception;
use Utils\Configuration\Config;

Class Db extends PDO {

    protected $connect_params = array ();

    protected $connect_params_options = array ();

    protected $configs = array ();

    public function __construct() {
        //Populate $this->configs with configs from the configuration class
        $this->configs = Config::get_config();
        //Populate the $connect_params array
        $this->assemble_connect_params();
        //Load the $this->connect_params_options array with PDO connection parameters
        $this->connect_params_options();

        list($dsn,$username,$password) = $this->connect_params;
        try {
            parent::__construct($dsn,$username,$password);
        }
        catch (PDOException $e) {
            throw $e;
        }
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: show_configs
    |--------------------------------------------------------------------------
    |  Debug: Fetch and Display the configs from the Config class
    |--------------------------------------------------------------------------
    */
    public function show_configs() {
        echo print_r($this->configs,true);
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: assemble_connect_params
    |--------------------------------------------------------------------------
    |  Populate the $connect_params array
    |--------------------------------------------------------------------------
    */
    protected function assemble_connect_params() {
        $dsn = $this->dsn();
        $this->connect_params = array ($dsn, $this->configs['database']['username'], $this->configs['database']['password']);
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: connect_params_options
    |--------------------------------------------------------------------------
    |  Populate the $connect_params_options array
    |--------------------------------------------------------------------------
    */
    protected function connect_params_options() {
        $this->connect_params_options = array (
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        );
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: dsn
    |--------------------------------------------------------------------------
    |  Produces the PDO dsn string
    |--------------------------------------------------------------------------
    */
    protected function dsn() {
        return 'mysql:dbname=' . $this->configs['database']['dbname'] . ';host=' . $this->configs['database']['host'];
    }
    
    /*
    |--------------------------------------------------------------------------
    |  Function Name: do_delete
    |--------------------------------------------------------------------------
    |  Utility function to perform deletes painlessly
    |--------------------------------------------------------------------------
    */
    public function do_delete($table, array $conditions) {
        try {
            ksort($conditions);
            $where = $this->prepare_where_values($conditions);
            print 'DELETE FROM ' . $table . ' WHERE ' . $where . PHP_EOL;
            $stmt = $this->prepare('DELETE FROM ' . $table . ' WHERE ' . $where);
            $exec_params = $this->collapse_params($conditions);
            if (!$stmt->execute($exec_params))
                throw new Exception($this->format_pdo_error($stmt->errorInfo()),41);
            return $stmt->rowCount();
        }
        catch (PDOException $pe) {
            print $pe->getMessage();
        }
        catch (Exception $e) {
            print $e->getMessage();
        }
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: do_update
    |--------------------------------------------------------------------------
    |  Utility function to perform updates painlessly
    |--------------------------------------------------------------------------
    */
    public function do_update($table, array $params, array $conditions) {
        try {
            ksort($params); ksort($conditions);
            $set_str = $this->prepare_set_values($params);
            $where = $this->prepare_where_values($conditions);
            print 'UPDATE ' . $table . ' SET ' . $set_str . ' WHERE ' . $where . PHP_EOL;
            $stmt = $this->prepare('UPDATE ' . $table . ' SET ' . $set_str . 'WHERE ' . $where);
            $exec_params = $this->collapse_params(array_merge($params,$conditions));
            if (!$stmt->execute($exec_params))
                throw new Exception($this->format_pdo_error($stmt->errorInfo()),41);
            return $stmt->rowCount();
        }
        catch (PDOException $pe) {
            print $pe->getMessage();
        }
        catch (Exception $e) {
            print $e->getMessage();
        }
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: collapse_params
    |--------------------------------------------------------------------------
    |  Utility function to collapse array of array values into a single 
    |  dimension array currently in use for the in clause
    |--------------------------------------------------------------------------
    */
    protected function collapse_params(array $arr) {
        $l = array();
        foreach (array_values($arr) as $item) {
            if (is_array($item) and count($item) > 0) {
                $li = $this->collapse_params($item);
                $l = array_merge($l,$li);
            }
            else {
                $l[] = $item;
            }
        }
        return $l;
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: prepare_set_values
    |--------------------------------------------------------------------------
    |  Constructs the update set string
    |--------------------------------------------------------------------------
    */
    protected function prepare_set_values(array $arr) {
        reset($arr); $set = '';
        while (list($k,) = each($arr)) {
            $set .= $k . ' = ? ';
            if (current($arr))
                $set .= ', ';
        }
        return $set;
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: prepare_where_in
    |--------------------------------------------------------------------------
    |  Construct the in part of a where clause
    |--------------------------------------------------------------------------
    */
    protected function prepare_where_in(array $arr) {
        $in = "";
        $in .= " IN (" . join(", ",array_fill(0,count($arr),'?')) . ") ";
        return $in;
    }

    /*
    |--------------------------------------------------------------------------
    |  Function Name: prepare_where_values
    |--------------------------------------------------------------------------
    |  Constructs the update where string
    |--------------------------------------------------------------------------
    */
    protected function prepare_where_values(array $arr) {
        $where = "";
        reset($arr);
        while (list($k,$v) = each($arr)) {
            if (is_array($v) and count($v) > 0) {
                $where .= $k . $this->prepare_where_in($v);
            }
            else {
                $where .= $k . " = ? ";
            }
            if (current($arr))
                $where .= "AND ";
        }
        return $where;
    }

    /* Returns a PDOStatement */
    public function select ( $query_str ) {
        return $this->query( $query_str );
    }

    /* Perform Inserts into the DB */
    public function insert ( $query_str ) {
        return $this->exec( $query_str );
    }

    /* Perform Update into the DB */
    public function update ( $query_str ) {
        return $this->exec( $query_str );
    }


    /*
    |--------------------------------------------------------------------------
    |  Function Name: format_pdo_error
    |--------------------------------------------------------------------------
    |  Formats a legible message from the array returned by PDOStatement::errorInfo()
    |--------------------------------------------------------------------------
    */
    protected function format_pdo_error(array $err) {
        $format = 'SQL Error: SQLERROR CODE [%d] - %s';
        return sprintf($format, $err[0], $err[2]);
    }
    
}
