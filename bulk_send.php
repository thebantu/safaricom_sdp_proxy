<?php

/*
require_once './resend/vendor/autoload.php';
require_once 'resend/Venom_bulk.php';
require_once 'resend/Requeue.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
*/

require_once 'resend/Venom_bulk.n.php';
require_once __DIR__ . '/vendor/autoload.php';
#require_once('./lib/SMSMisc.php');

Logger::configure('bulk_configsms.xml');
$logger = Logger::getLogger("SDPSENDSMS");

/* --------------------------------------------------------------------------------
 * Set options for the SOAP Object
 * --------------------------------------------------------------------------------
 */
ini_set('soap.wsdl_cache_enabled', 1);
ini_set('soap.wsdl_cache_ttl', 88640);
ini_set('default_socket_timeout', 180);
ini_set('max_execution_time', 120);
/* ------------------------------------------------------------------------------- */

/*
 * -------------------------------------------------------------------------------------------------------------------
 * Parse meta data regex
 * --------------------------------------------------------------------------------
 */

$purge_heading = '/\?[^?]+\?/';
$meta_data = preg_replace($purge_heading, "", urldecode($_GET['meta_data']));

$logger->info("This is the metadata string: " . $meta_data);
 $regex = '/sdp_service_id=([^&]+)&correlator=([^&]+)(?:&(?:link_id|callback)=([^&]+))?(?:&(?:long_message|req_dlr)=(.+$))?/s';
 $brk_dwn = [];
 
 /*
  * Match regex against urldecode metadata string
  * and harvest the param values
  */
 
 if (preg_match_all($regex, $meta_data, $brk_dwn)) {
  $kmp_service_id = $brk_dwn[1][0];
  $kmp_correlator = $brk_dwn[2][0];
  $correlator = preg_replace('/-/', '', $kmp_correlator);
  $callback = $brk_dwn[3][0] ?: "NOT SET";
  $long_message = $brk_dwn[4][0];
 }

/* -------------------------------------------------------------------------------
 * Handle kannel metadata using regex.
 * -------------------------------------------------------------------------------
 * TODO: Need to change this to use the more safe php native parse_str.
 * -------------------------------------------------------------------------------


 * parse_str( $meta_data, $brk_dwn);
 * 
 * $kmp_service_id = $brk_dwn['sdp_service_id'];
 * $kmp_correlator = $brk_dwn['correlator'];
 * $correlator = preg_replace('/-/', '', $kmp_correlator);
 * $kmp_linkid = "NOT SET";
 * if(isset($brk_dwn['link_id'])){
 * $kmp_linkid = $brk_dwn['link_id'] ?: "NOT SET";
 * }
 * $long_message ="NOT SET";
 * if(isset($brk_dwn['long_message'])){
 * $long_message = $brk_dwn['long_message'] ?: "NOT SET";
 * }
 */
$logger->info("I have parsed the following from metadata => " . print_r($brk_dwn, true));

$start = startTimer();


$kmp_code = $_GET['short_code'];
$kmp_recipients = $_GET['msisdn'];
/*
if ($kmp_recipients == '254726986944' ) {
    print "retry_msg";
    header("HTTP/1.1 203 Retry Message");
    die;
}
*/
$message = $_GET['message'];
if ($message == 'CONCT')
	$message = $long_message;

/* Parse settings from the main configuration file */
$iniarray = parse_ini_file(__DIR__ . '/configuration.ini', TRUE);

$kmp_spid = substr($kmp_service_id, 0, 6);

/* Set the SDP Password */
$kmp_password = $iniarray['SMS_SDP'][$kmp_spid];
$logger->info("Password [ $kmp_spid ] from Configuration is:::::: -> $kmp_password SERVICEID:::  [ $kmp_service_id ]");

$client = new SoapClient(__DIR__ . '/wsdl/parlayx_sms_send_service_2_2.wsdl', array('trace' => 1));
$ep = $iniarray['SMS_SDP']['endpoint'];
$client->__setLocation($ep);
$dlr_ep = $iniarray['SMS_SDP']['bulk_dlr_endpoint'];
$now = date("YmdGis");
$pass = md5("$kmp_spid$kmp_password$now");
$auth = new stdClass();
$auth->spId = $kmp_spid;
$auth->spPassword = $pass;
$auth->serviceId = $kmp_service_id;
$auth->timeStamp = $now;

$ns = "http://www.huawei.com.cn/schema/common/v2_1";
$header = new SoapHeader($ns, 'RequestSOAPHeader', $auth, false);
$client->__setSoapHeaders($header);

$kmp_message = preg_replace('/[^(\x20-\x7F)]*/', '', trim(rawurldecode($message)));
$logger->info("SEND SMS  $kmp_code| $kmp_message | $kmp_recipients | $kmp_service_id | $kmp_spid | $dlr_ep");
$dlr_request = array (
		"receiptRequest" => array
		(
		 "endpoint" => $dlr_ep,
		 "interfaceName" => "SmsNotification",
		 "correlator" => $kmp_correlator
		)
		);
$sendSMS = [
            "sendSms" => [
                             "addresses" => "tel:$kmp_recipients",
                             "senderName" => $kmp_code,
                             "message" => $kmp_message,
                         ]
		   ];
//$sendSMS["sendSms"] = array_merge($sendSMS["sendSms"], $dlr_request);

/* Submit the message to Safaricom */
try {
	$results = $client->__soapCall("sendSms", $sendSMS);
	$logger->info($client->__getLastRequest());
} catch (SoapFault $soapFault) {
	$logger->info($client->__getLastRequest());
	$result = "not done|$link";

	$response = "Error| $soapFault->faultcode | $soapFault->faultstring |CORR: $kmp_correlator | CODE: $kmp_code |SERVICEID:  $kmp_service_id | SPID: $kmp_spid ";
	$logger->error($response . "|ERROR OBJECT|" . json_encode($soapFault));
	echo $response;
}
if ( !empty($results) ) {
	$result = processSendSms($kmp_message,$results, $kmp_recipients, $kmp_correlator, $kmp_message,  $start, $kmp_code,$kmp_service_id,$kmp_password,$kmp_spid);
} else {
        /* Permanent Failure Regex */
        $temp_failure_error = '/' .
                              /* Network error of some sort */
                              '(Could not connect to host)' .

                              /* Another network error */
                              '|(Error Fetching http headers)' .

                              /* Abusing the SLA by exceeding the configured throughput */
                              '|(SP API level request rate control not pass, sla id is \d+)' .

                              /* Abusing the SLA by exceeding the configured
                                 throughput 2
                               */
                              '|(SP level gross control not pass.\d+)' .

                              /* Need to confirm this error */
                              '|(The message has been static flow control)' .

                              /* Correlator Error */
                              /* '|(Correlator \d+ has a invalid format)' . */

                              /* Perform case insesitive match */
                              '/i';
        if (preg_match($temp_failure_error, $response)) {
                echo "retry_msg";
                header("HTTP/1.1 203 Retry Message");
        }
        else {
                echo "perm_failure";
                header("HTTP/1.1 500 Message Sending Failed Permanently");
        }
}

function processSendSms($text, $results, $kmp_recipients, $kmp_correlator, $kmp_message,  $start, $kmp_code,$kmp_service_id,$password,$spid) {
	global $logger;
	try {
		$result = "";
		$totaltime = stopTimer($start);
		if ( !empty($results->result) ) {
			$res = $results->result;
			/*
			Changed the returned string from 
			  returning the request id to returning
			  the correlator instead.
			$result .= "ok <ID>$kmp_correlator</ID>";
			*/
			$result .= "ok <ID>$res</ID>";
			/*
			Do not! queue
			*/
			$dlr = array(
					'requestIdentifier'=>$res,
					'msisdn'=>$kmp_recipients,
					'serviceId'=>$kmp_service_id,
					'shortcode'=>$kmp_code,
					'correlator'=>$kmp_correlator,
					'password'=>$password,
					'spid'=>$spid,
            );

            /* This piece of code logs the dlr request into a database */
            //$misc_sms = new SMSMisc("dlr_requests_bulk");
            //$misc_sms->log_dlr_request( $dlr );

            /* This piece of code publishes directly to a queue
                this piece of code also ostensibly slows this script down
                to a halt, when high volumes of SMS's need to be sent out.
                From preliminary investigations connecting to rabbitmq is
                an expensive affair that causes this script to manifest
                that problem.
            **/
                    $vnom = new Venom();
                    $vnom->actionPublish(json_encode($dlr));
            


		}else{

			$result = "not done";

		}
		$totaltime = stopTimer($start);	
		$response = "done in $totaltime| code $kmp_code | msisdn: $kmp_recipients | correlator: $kmp_correlator| res: $res " . json_encode($results);
		$logger->info($response);
		echo $result;

	} catch (Exception $exc) {
		$logger->error($exc->getMessage());
		exit($exc->getMessage());
	}
	return $result;
}

function startTimer() {
	$mtime = microtime();
	$mtime_r = explode(" ", $mtime);
	$time = $mtime_r[1] + $mtime_r[0];
	return $time;
}

function stopTimer($start) {
	$mtime = microtime();
	$mtime_r = explode(" ", $mtime);
	$time = $mtime_r[1] + $mtime_r[0];
	$endtime = $time;
	$totaltime = ($endtime - $start);
	return $totaltime;
}

